package com.example.demo;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.runners.MethodSorters;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.*;
import static org.hamcrest.Matchers.containsString;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.example.model.customerTable;
import com.example.repo.customerRep;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.boot.test.rule.OutputCapture;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringRunner.class)//U can choose any Other Runner classes like junit4Runner etc here
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)//For Every it will give you a random port number
@FixMethodOrder(MethodSorters.NAME_ASCENDING)//It will take your test methods in assending order
public class mockTest {
/*
    private MockMvc mockMvc;//This will mock your restemplates

    @Autowired
    private TestRestTemplate restTemplate = new TestRestTemplate();

    @Autowired
    private WebApplicationContext wac;

    @Rule
    public OutputCapture capture = new OutputCapture();

    @Autowired
    private customerRep cusR;

    @Autowired
    private customerController controller;

    @InjectMocks
    private customerTable c1=new customerTable();

    public int i=64;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();

    }
    @Test
    public void a_verifyConnection() throws Exception {
        try {
            mockMvc.perform(get("/customer/all")
                    .accept(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andDo(print());
        } catch (java.lang.Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void b_postMethod() throws Exception{
        try{

            Long l=Long.valueOf(i);
            customerTable c1=new customerTable();
            c1.setAge(20);
            c1.setName("Mel");
            c1.setId(l);
            ObjectMapper map=new ObjectMapper();
            map.configure(SerializationFeature.WRAP_ROOT_VALUE,false);
            ObjectWriter ow=map.writer().withDefaultPrettyPrinter();
            String Json=ow.writeValueAsString(c1);
            mockMvc.perform(MockMvcRequestBuilders.post("/customer/")
                    .contentType(APPLICATION_JSON_UTF8)
                    .content(Json))
                    .andExpect(status().is2xxSuccessful())
                    .andDo(print());
        }catch (Exception e){
            e.printStackTrace();
        } catch (java.lang.Exception e) {
            e.printStackTrace();
        }
    }


    @Test
    public void c_getById() throws Exception{
        int idNum=i;
        try{
            mockMvc.perform(get("/customer/"+idNum).accept(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(content().string(containsString("{\"id\":"+idNum+",\"name\":\"Mel\",\"age\":20}")))
                    .andExpect(jsonPath("$.id").value(idNum))
                    .andDo(print());

        }catch (Exception e){
            e.printStackTrace();
        } catch (java.lang.Exception e) {
            e.printStackTrace();
        }
        finally {

        }

    }

    @Test
    public void d_putMethod() throws Exception{
        try{
            int idNum=i;
            int expectedAge=21;
            mockMvc.perform(put("/customer/"+idNum).accept(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.age").value(expectedAge))
                    .andDo(print());
        }catch (Exception e){
            e.printStackTrace();
        } catch (java.lang.Exception e) {
            e.printStackTrace();
        }
    }







    @Test
    public void e_test1() throws Exception{


        Long l=Long.valueOf(i);
        customerTable c1=controller.getUser(l);
        //c1.setName("Roy");

        assertThat(c1.getName()).isEqualTo("Mel");


    }

    @Test
    public void f_delMethod() throws Exception{
        try{
            int idNum=i;
            mockMvc.perform(delete("http://localhost:8080/customer/"+idNum).accept(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andDo(print());
        }catch (Exception e){
            e.printStackTrace();
        } catch (java.lang.Exception e) {
            e.printStackTrace();
        }
    }

*/

    @Test
    public void g1()throws Exception{
        assertThat("1").isEqualTo("1");
    }

}
